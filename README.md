# DDEV openssh-server docker-compose configuration

## Description
This is an attempt to define a docker-compose config file that adds 
an openssh-server container to a ddev-based site.

## Status
WORK-IN-PROGRESS. Does not currently work correctly:
* does not accept password
* probably does not actually help you actually get into your ddev site's web container

## Usage

* place docker-compose.openssh.yaml in your ddev site's `.ddev/` directory
* `ddev restart`
* connect from your host with `ssh root@YOUR-DDEV-SITENAME -p2222`
* use the password that's set in docker-compose.openssh.yaml